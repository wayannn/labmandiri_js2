//Array
const nama = ["Wayan", "Agus", "Aditya"];
console.log(nama);

const umur = [23];
console.log(umur);

const gabung = nama.concat(umur);
console.log(gabung);

gabung.splice(3, 0, 1997);
console.log(gabung);

const isiArray = (nArray) => {
  for (let iterasi = 0; iterasi < nArray.length; iterasi++) {
    let element = nArray[iterasi];
    console.log(element);
  }
};

isiArray(gabung);

let input = document.getElementById("input");

//fungsi kalkulator
const tambahKurang = (n) => {
  hasil = parseInt(input.value) + n;
  input.value = hasil;
};

//konversi suhu
let input_suhu = document.getElementById("nilai_suhu");
let tombol_ubah = document.getElementById("tombol_ubah");
let hasil_suhu = document.getElementById("hasil_suhu");

tombol_ubah.onclick = () => {
  ubahSuhuFkeC(input_suhu.value);
  tombol_ubah.innerHTML = "Ubah ke Fahrenheit";
};

//fungsi Fahrenheit ke Celcius
const ubahSuhuFkeC = (fahrenheit) => {
  hasil = (5 / 9) * (fahrenheit - 32);
  hasil_suhu.innerHTML = hasil ;
};

//fungsi Celcius ke Fahrenheit
const ubahSuhuCkeF = (celcius) => {
  hasil = (9 / 5) * (celcius + 32);
  hasil_suhu.innerHTML = hasil;
};
